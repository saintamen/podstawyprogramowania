package exerciseLombok;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Student {
    private String name = "dżordż";
    private int age;
}
