package exerciseLombok;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Journal {
    private List<Student> studentList = new ArrayList<>();

    public void addStudent(Student t) {
        studentList.add(t);
    }

    public Optional<Student> getStudentAtIndex(int index) {
        if (studentList.size() <= index) {
            return Optional.empty();
        } else if (index < 0) {
            return Optional.empty();
        }

        return Optional.ofNullable(studentList.get(index));
    }
}
