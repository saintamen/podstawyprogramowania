package exerciseLombok;

import java.util.Optional;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        Journal j = new Journal();
        j.addStudent(new Student());
        Optional<Student> st = j.getStudentAtIndex(0);

        if (st.isPresent()) {
            Student student = st.get();
            System.out.println(student.getName());
        }

        Consumer<Student> consumer = new Consumer<Student>() {
            @Override
            public void accept(Student student) {
                System.out.println(student.getName());
            }
        };

        st.ifPresent(consumer);

        st.ifPresent(student -> {
            System.out.println(student.getName());
        });
    }
}
