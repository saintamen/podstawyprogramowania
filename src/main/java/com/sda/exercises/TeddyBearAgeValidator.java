package com.sda.exercises;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class TeddyBearAgeValidator {
    public static boolean validate(Object o) throws IllegalAccessException {
        // z obiektu pobieram jego pola
        Field[] fields = o.getClass().getDeclaredFields();
        // dla każdego pola robie sprawdzenie czy posiada moja adnotacje
        for (Field field : fields) {
            MinMaxValue annotation = field.getAnnotation(MinMaxValue.class);
            if (annotation != null) {
                // dla adnotacji weryfikuje wartość

                field.setAccessible(true);
                int age = (int) field.get(o);
                if (age < annotation.minValue() || age > annotation.maxValue()) {
                    return false;
                }
                field.setAccessible(false);
            }
        }
        return true;
    }
}
